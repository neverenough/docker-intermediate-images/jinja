# Docker Jinja

This image is meant to be used as intermediate image during the build of other images. It integrates Jinja and provides some useful scripts to recursively render all the configuration files inside a specific folder.

## Security
This image should only be used in intermediate images so that the final image doesn't have any trace or weakness of jinja.

## Getting Started

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

It should only be used in intermediate images. Despite that, in the following sections we also explain how it can be used to manually render some configuration files.

#### Build

To build a server with the default configuration, simply build the image:

```shell
$ docker build . -t jinja:latest
```

#### Use as intermediate image
```dockerfile
RUN /app/entrypoint.sh -i INPUT_DIR -o OUTPUT_DIR -c
```

#### Run

By default, it launches a custom applications that guides the user to correctly use the tool.
To run the image in such a way:

```shell
$ docker run jinja:latest
```

If you want to launch Jinja with the minimum options:

```shell
$ docker run jinja:latest jinja -i INPUT_DIR -o OUTPUT_DIR
```

However, if you want to execute arbitrary command inside the container (testing/debugging), you can concatenate such instruction to the launch command

```shell
$ docker run -it jinja:latest /bin/bash echo "Hello world!"
```

#### Build Arguments
This image doesn't require any argument by itself.
Despite that, it should be used ad intermediate image, and this kind of images only access the arguments provided during the build phase. For this reason it is strongly recomended that you define all the required variables as arguments.

#### Execution options

* [ *optional* ] `-h` `--help` - prints an help message;
* [ *required* ] `-i` `--input` - the input directory;
* [ *required* ] `-o` `--output` - the output directory;
* [ *optional* ] `-c` `--copy-others` - wheather it should copy to the destination folder all the non-jinja files or not;
* [ *optional* ] `-l` `--level` - the debug level. Default: *INFO*. Other options: *ERROR*, *DEBUG* ...

#### Useful File Locations

Some useful files can be found in:
* `lib` - in this directory are contained all the custom scripts written to handle the jinja renderer.

## Find Us

* [GitHub](https://gitlab.com/neverenough/docker-images/jinja)

## Authors

* **Eugenio Pierfederici** - *Initial work* - [GitHub](https://github.com/iuginP) [Gitlab](https://gitlab.com/iugin)
* **Eugenio Cavina** - *Contributions* - [Gitlab](https://gitlab.com/ecavina)
