#!/bin/bash

if [ "$1" = 'jinja' ]; then
    shift
    # Executes the python program with the provided arguments
    set -- python3 /app/lib/main.py "$@"
    if [ $? -ne 0 ]; then
        exit 1
    fi
fi

exec "$@"