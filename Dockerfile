FROM debian:buster-slim

LABEL maintainer="iugin"

# Using pipe fails on any command failed
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Clean output only during build
ARG DEBIAN_FRONTEND=noninteractive

# Install Jinja
RUN set -e -x && \
    deps="python3-pip python3-setuptools" && \
    apt-get update && apt-get install --no-install-recommends -y $deps
RUN pip3 install --upgrade \
    setuptools \
    Jinja2

# Clean image
RUN set -e -x && \
    apt-get purge -y --auto-remove \
        $deps && \
    rm -rf \
        /tmp/* \
        /var/tmp/* \
        /var/lib/apt/lists/*

# Copy the python files and the initial script
COPY lib /app/lib
COPY entrypoint.sh /app/entrypoint.sh

WORKDIR /app

ENTRYPOINT [ "/app/entrypoint.sh" ]

CMD [ "jinja" ]