from jinja2 import Environment, FileSystemLoader, select_autoescape
import os
from utilities import rchop
import logger

class Renderer:

  def __init__(self, templates_directory):
    self.log = logger.getLogger("jinja")
    # Create the environment to load the templates from file instead of string
    self.env = Environment(
      loader = FileSystemLoader(templates_directory)
    )

    # Set the filter to use the custom sys_env
    self.env.filters['get_env_or'] = self.__get_env_or
    self.env.filters['get_env'] = self.__get_env
    self.env.filters['env_not_empty'] = self.__env_not_empty
    self.env.filters['env_is'] = self.__env_is
    self.env.filters['get_env_list'] = self.__get_env_list

  # Custom filter for intercepting the system environment variables
  def __get_env_or(self, key, value):
    return os.getenv(key, value)

  def __get_env(self, key):
    return os.getenv(key, "")

  # Assert the env is defined, non empty or just white spaces
  def __env_not_empty(self, key):
    return os.getenv(key) is not None and len(os.getenv(key).strip()) > 0

  def __env_is(self, key, value):
    return os.getenv(key) is not None and os.getenv(key) == value
  
  def __get_env_list(self, key):
    return [e for e in map(str.strip, self.__get_env(key).split(";")) if len(e) > 0]

  # Rendering method: returns the rendered file
  def render(self, template_file):
    self.log.info(f"Rendering: {template_file}")
    return self.env.get_template(template_file).render()


class RendererToFile(Renderer):

  def __init__(self, templates_directory, output_directory):
    super().__init__(templates_directory)
    self.output_directory = output_directory

  def render_to_file(self, template_file):
    # Render the file
    self.log.info(f"Rendering {template_file} to file ...")
    rendered = self.render(template_file)

    # Compute the output file path
    output_file = None
    try:
      if template_file.endswith(".j2"):
        output_file = os.path.join(self.output_directory, rchop(template_file, ".j2"))
      elif template_file.endswith(".jinja"):
        output_file = os.path.join(self.output_directory, rchop(template_file, ".jinja"))
      else:
        output_file = os.path.join(self.output_directory, template_file)
    except Exception as ex:
      self.log.exception(f"An exception occurred while computing the output path for {template_file}")
      return False

    try:
      # Save the result to file
      with open(output_file, "w") as fh:
        fh.write(rendered)
      self.log.info(f"Rendered {template_file} to {output_file}")
      return True
    except Exception as ex:
      self.log.exception(f"An exception occurred while writing the render of {template_file} to {output_file}")
      return False
