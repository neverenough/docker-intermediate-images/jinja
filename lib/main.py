#!/usr/bin/env python3

# $1: template dir (input)
# $2: config dir (output)

from renderer import RendererToFile
import logger
from logger import Logger
from pathlib import Path
from os import path
from shutil import copyfile
import getopt, sys

# Recursively renders all the templates from the input dir to the output dir
# if it finds any file of different format it may copy them to the destination (copy_others)
def recursively_render(input_dir, output_dir, copy_others):
    logging = logger.getLogger('jinja')
    # Define the jinja renderering environment
    logging.debug('Defining the jinja renderering environment...')
    jinja_renderer = RendererToFile(input_dir, output_dir)
    # Recursive search of every configuration file in the input directory
    logging.debug('Recursive searching of every configuration file in the input directory...')
    configuration_files = list(Path(input_dir).rglob('*'))
    # Iterates over every file and render it to the output directory
    logging.info("Start rendering...")
    for configuration_file in configuration_files:
        if configuration_file.is_file():
            # Extract the relative path to the file (relative to the input_dir)
            relative_path = str(path.relpath(configuration_file, input_dir))
            if relative_path.endswith(".j2") or relative_path.endswith(".jinja"):
                # Avoid conversion of abstract files:
                # Those files are only extended or used by other child templates
                if not (relative_path.endswith(".abstract.j2") or relative_path.endswith(".abstract.jinja")):
                    jinja_renderer.render_to_file(relative_path)
            elif copy_others:
                copyfile(configuration_file, path.join(output_dir, relative_path))

# Prints the help message explaining how to use the script
def print_arguments_help():
    print('''main.py -i <inputdir> -o <outputdir> -l <level>
    -h  --help          Print a help message.
    -l  --level         The level at witch the messages must be printed;
                        It is one of: DEBUG, INFO, WARNING, ERROR, CRITICAL.
    -i  --input         The input directory of all the templates.
    -o  --output        The output directory that will contain
                        the rendered configuration files.
    -c  --copy-others   Copy to the destination directory all those files
                        that jinja doesn't recognizes.
        ''')

def main(argv):
    inputdir = ''
    outputdir = ''
    copy_others = False
    level = "INFO"
    try:
        opts, args = getopt.getopt(argv,"hcl:i:o:",["help", "copy-others", "level", "input=","output="])
    except getopt.GetoptError:
        print_arguments_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_arguments_help()
            sys.exit()
        elif opt in ("-c", "--copy-others"):
            copy_others = True
        elif opt in ("-l", "--level"):
            level = arg
        elif opt in ("-i", "--input"):
            inputdir = arg
        elif opt in ("-o", "--output"):
            outputdir = arg

    logging = Logger("jinja", level).logger
    logging.info(f'VERBOSE    : {level}')
    logging.info(f'INPUT      : {inputdir}')
    logging.info(f'OUTPUT     : {outputdir}')
    logging.info(f'COPY_OTHERS: {copy_others}')

    if inputdir == '' or outputdir == '':
        logging.critical('Error! Missing options in command.')
        print_arguments_help()
        sys.exit(2)

    recursively_render(inputdir, outputdir, copy_others)

    logging.info(f'All rendering completed!')

if __name__ == "__main__":
    main(sys.argv[1:])
