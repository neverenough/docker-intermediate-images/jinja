import logging

class Logger:
  def __init__(self, app, level, file_log = False):
    self.logger = logging.getLogger(app)
    self.logger.setLevel(level)

    console_handler = logging.StreamHandler() 
    console_format = logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s - %(message)s')
    console_handler.setFormatter(console_format)
    self.logger.addHandler(console_handler)

    if file_log:
      file_handler = logging.FileHandler(app + '-jinja.log')
      file_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
      file_handler.setFormatter(file_format)
      self.logger.addHandler(file_handler)

def getLogger(app):
  return logging.getLogger(app)
